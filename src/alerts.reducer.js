
export default function alertReducer(state = {}, action) {
    switch (action.type) {
        case 'is_loading':
            return {...state,is_loading:action.is_loading};
    
        default:
            return {is_loading:false}
            break;
    }
}