import React from 'react'
import { connect } from 'react-redux'
import { WebService } from '../services/WebService';
const Homes = (props) => {
    
    const {dispatch,is_loading} = props;

    const callAPi = () => {
        const apiEndpoint = 'https://jsonplaceholder.typicode.com/todos/';
        let header = WebService.getHeaders({})
        //console.log(postArray)
        dispatch({ type:'is_loading',is_loading:false})
        WebService.get(apiEndpoint, header).then((response) => {
            console.log(response.data);
            dispatch({ type:'is_loading',is_loading:true})
            console.log('=====is_loading====',is_loading);
        })
    }
    return (
    
            <button onClick={() => callAPi()}>
                call api {is_loading +' comes '}
            </button>
    
    )
}

const mapStateToProps = (state /*, ownProps*/) => {
    console.log('=====state========',state);
    const { is_loading } = state.alertReducer;
    return {
        is_loading
    }
  }
   
 const Home =  connect(
    mapStateToProps
  )(Homes)

export default Home
