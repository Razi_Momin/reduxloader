
import { combineReducers } from 'redux';

import alertReducer from './alerts.reducer';

export default combineReducers({alertReducer});