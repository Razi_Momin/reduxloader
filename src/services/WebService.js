import axios from 'axios';



export const WebService = {
    get,
    getHeaders
};

function get(apiEndpoint, headers) {
    return axios
        .get(apiEndpoint, { headers })
        .then((response) => {
            return response;
        })
        .catch((err) => {
            return ErrorRespond(err);
        });
}




function ErrorRespond(error) {
    return { data: { type: 'ERROR', message: error, status: 'NET_ERROR' } };
}


function getHeaders(otherHeader = []) {
    const auth = JSON.parse(localStorage.getItem('auth'));
    let key = auth ? auth.key : '';
    let token = auth ? auth.token : '';
    let ppm_id = auth ? auth.ppm_id : 0;
    let options = {
        // // Accept: 'application/json',
        'Access-Control-Allow-Origin': '*',
        "Content-Type": "application/x-www-form-urlencoded",
        'Client-Service': 'mobile-client-qc',
        'Auth-key': 'playerzpotrestapi',
        'ID': ppm_id,
        'Platform': 1,
        'Authorization': token,
        'PPM-API-KEY': key,
        'Game-Mode': 1
    };
    // console.log(options);
    options = { ...otherHeader, ...options };
    return options;
}